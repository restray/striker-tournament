<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'beta');

Route::get('subscribe', 'Beta@subscribe')->name('subscribe');
Route::post('subscribe/store', 'Beta@store')->name('subscribe-store');

Auth::routes();


Route::group(['prefix' => 'access', 'middleware' => 'auth'], function() {
    // Index
    Route::get('/', 'HomeController@index')->name('home');
    // Overwatch checkup
    Route::get('ow', 'Auth\BlizzardAuthentification@redirectToProvider_BattleNet')->name('overwatch-connect');
    Route::get('ow/callback', 'Auth\BlizzardAuthentification@handleProviderCallback_BattleNet')->name('overwatch-connect-callback');
    // Steam checkup
    Route::get('auth/steam', 'Auth\SteamAuthentification@redirectToSteam')->name('auth-steam');
    Route::get('auth/steam/handle', 'Auth\SteamAuthentification@handleSteam')->name('auth-steam-handle');
});