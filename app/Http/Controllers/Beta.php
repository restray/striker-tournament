<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BetaUser;

class Beta extends Controller
{
    public function subscribe(){
        return view('subscribe');
    }
    public function store(Request $request){
        \Validator::make($request->all(), [
            'surname' => 'required|max:100',
            'name' => 'required|max:100',
            'email' => 'required|unique:beta_users|email',
            'adress' => 'required|max:255',
            'pseudo' => 'required|unique:beta_users|max:50',
            'birthday' => 'required|date',
        ])->validate();

        $user = new BetaUser;

        $user->pseudo = $request->input('pseudo');
        $user->email = $request->input('email');
        $user->surname = $request->input('surname');
        $user->name = $request->input('name');
        $user->adress = $request->input('adress');
        $user->birthday = $request->input('birthday');

        $user->save();

        return "<html><head><meta http-equiv=\"refresh\" content=\"5; url=". route('http://example.com/'). "\" /></head><body>Vous allez être redirigé.</body></html>";
    }
}
