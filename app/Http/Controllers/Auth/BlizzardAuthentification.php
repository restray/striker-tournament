<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Auth;
use App\User;

use App\Http\Controllers\Controller;

class BlizzardAuthentification extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Overwatch/blizzard Oauth 
    public function redirectToProvider_BattleNet()
    {
        return redirect(\BattleNetApi::authenticationURL()); // redirect to BattleNet login page
    }
    public function handleProviderCallback_BattleNet(Request $request)
    {
        $social_type = "BattleNet";

        if (isset($_GET['code'])) {
            $code = $_GET['code'];

            $token = \BattleNetApi::requestToken($code);

            $account = \BattleNetApi::authenticatedUser($token);

            // Change user battletag info
            $user = User::where('id', Auth::user()->id)->first();

            $user->battletag = $account['battletag'];
            $user->checkBlizzardGame();

            $user->save();

            return redirect()->route('home');
        } else
            return redirect()->route('home')->withErrors('failed.');

    }
    
}
