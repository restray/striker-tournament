<?php

namespace App\Http\Controllers\Auth;

use Invisnik\LaravelSteamAuth\SteamAuth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Controllers\Controller;

class SteamAuthentification extends Controller
{
    /**
     * The SteamAuth instance.
     *
     * @var SteamAuth
     */
    protected $steam;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $redirectURL = '/access';

    /**
     * AuthController constructor.
     * 
     * @param SteamAuth $steam
     */
    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
        $this->middleware('auth');
    }

    /**
     * Redirect the user to the authentication steam page
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToSteam()
    {
        return $this->steam->redirect();
    }

    /**
     * Get user info and save it to the connected user
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleSteam()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                $user = User::where('id', Auth::user()->id)->first();

                $user->steamid = $info->steamID64;
                $user->checkSteamGames();

                $user->save();

                return redirect($this->redirectURL); // redirect to site
            }
        }
        return $this->redirectToSteam();
    }
}
