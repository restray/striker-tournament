<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Create the firsts roles.
     *
     * @return \Illuminate\Http\Redirect
     */
    public function init_roles(){
        // Create the admin role
        $role = Role::create(['name' => 'admin']);

        // Create the first permission
        $permission = Permission::create(['name' => 'view adminpanel']);
        $permission->assignRole($role);

        // TODO : work with migrations
    }
}
