<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'battletag', 'steamid', 'csgo', 'overwatch', 'rainbowsix', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the Steam's games of a User and check these games.
     * 
     * @return boolean if the user have a steamid
     */
    public function checkSteamGames(){
        if ($this->steamid != null){
            // Create the request to the owapi
            $ch = curl_init("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=". \Config::get('steam-auth.api_key') ."&steamid=". $this->steamid ."&format=json");

            // Set the opt curl
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            // Get the data
            $data = json_decode(curl_exec($ch), true);

            // Close the request
            curl_close($ch);
            
            // Check CSGO
            $hasCsgo = $this->checkCSGOGame($data['response']['games']);
            // Save the game
            $this->csgo = $hasCsgo;
            $this->save();

            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Check the CSGO games
     *
     * @param Array $games
     * @return Array
     */
    private function checkCSGOGame(Array $games){
        $check = [];
        foreach($games as $game){
            if($game['appid'] == 730){
                return true;
            }
        }
        return false;
    }

    /**
     * Get all the games
     *
     * @return boolean if the user have Overwatch 
     */
    public function checkBlizzardGame(){
        if ($this->battletag != null){
            $battletag = str_replace("#", "-", $this->battletag);

            // Create the request to the owapi
            $ch = curl_init("http://62.4.9.220:4444/api/v3/u/". $battletag ."/blob");
            // Set the opt curl
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HEADER, false);

            // Get the data
            $data = json_decode(curl_exec($ch));
            // Close the request
            curl_close($ch);

            // Update the user
            if (isset($data->eu)){
                // Save the game
                $this->overwatch = true;
                $this->save();

                // Redirect the user to the index
                return true;
            }
        }
        return false;
    }
}
