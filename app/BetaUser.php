<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BetaUser extends Model
{
    protected $table = "beta_users";
}
