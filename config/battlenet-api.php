<?php

return [
    'region' => env('Battle_net_region', 'eu'),
    'api_url' => "https://". env('Battle_net_region', 'eu') .".api.battle.net",
    'api_url_cn' => "https://api.battle.com.cn/",
    'client_id' => env('Battle_net_client_id', '5w8pv9bkcurtz3ehzstxbbqdrypprjsd'),
    'client_secret' => env('Battle_net_client_secret', 'FfyBT6fJDzBSU3MDNCaKKxE8wkJA82JC'),
    'redirect_url' => env('APP_URL') . '/access/ow/callback',
    'scopes' => [
        'wow.profile',
        'sc2.profile'
    ]
];