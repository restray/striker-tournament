<?php

return [

    /*
     * Redirect URL after login
     */
    'redirect_url' => '/access/auth/steam/handle',
    /*
     * API Key (set in .env file) [http://steamcommunity.com/dev/apikey]
     */
    'api_key' => env('STEAM_API_KEY', '7D10116E8AE7C8F5C12D5F12BA1F6B40'),
    /*
     * Is using https ?
     */
    'https' => true,

];
