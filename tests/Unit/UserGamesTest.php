<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\SteamAuthentification;

class UserGamesTest extends TestCase
{
    public function testNotSteamID()
    {
        // Create the User
        $userCSGO = factory(\App\User::class)->make();

        // Check the game of the user
        $resultTest = $userCSGO->checkSteamGames();

        // Check if player have CSGO
        $this->assertFalse($resultTest);
    }

    public function testCSGOGames()
    {
        // Create the User
        $userCSGO = factory(\App\User::class)->make([
            'steamid' => '76561197960434622',
        ]);

        // Check the game of the user
        $resultTest = $userCSGO->checkSteamGames();

        // Check if player have CSGO
        $this->assertTrue($userCSGO->csgo);
    }

    public function testNotCSGOGames()
    {
        // Create the User
        $userNotCSGO = factory(\App\User::class)->make([
            'steamid' => '76561198119897975',
        ]);

        // Check the game of the user
        $resultTest = $userNotCSGO->checkSteamGames();

        // Check if player have CSGO
        $this->assertFalse($userNotCSGO->csgo);
    }

    public function testNotBlizzardAccount(){
        // Create the User
        $userOverwatch = factory(\App\User::class)->make([
            'battletag' => 'Restray#21',
        ]);

        // Check the game of the user
        $resultTest = $userOverwatch->checkBlizzardGame();

        // Check if player have CSGO
        $this->assertFalse($resultTest);
    }

    public function testOverwatchGame(){
        // Create the User
        $userOverwatch = factory(\App\User::class)->make([
            'battletag' => 'Restray#21985',
        ]);

        // Check the game of the user
        $resultTest = $userOverwatch->checkBlizzardGame();

        // Check if player have CSGO
        $this->assertTrue($resultTest);
    }

    
}
