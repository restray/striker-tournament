@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="oauth">
                        <p><a href="{{ route('auth-steam') }}">Se connecter à Steam</a> | Steam : {{ Auth::user()->steamid }}</p>
                        <p><a href="{{ route('overwatch-connect') }}">Se connecter à Blizzard</a> | Blizzard : {{ Auth::user()->battletag }}</p>
                    </div>
                    <div class="games">
                        <p>Overwatch : {{ Auth::user()->overwatch == true ? "Oui" : "Non" }}</p>
                        <p>CS:Go : {{ Auth::user()->csgo == true ? "Oui" : "Non" }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
