<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Days+One|Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/beta.css') }}">
</head>
<body>
    <div class="container">
        <div class="h-100 row align-items-center">
            <div class="col-12 col-md-6">
                <div id="logo" class="ml">
                    Logo
                </div>
                <div id="addition" class="ml mt">
                    BLABLA + BLABLA = Soupe de blabla
                </div>
                <div id="slogan" class="ml mt">
                    Nous sommes les meilleurs et nous le resterons parce que les autres n'y arriveront pas.
                </div>
                <div class="text-center mt">
                    <a href="{{ route('subscribe') }}">
                        <span class="arrow-right"></span>S'inscrire
                    </a>
                </div>
                
            </div>
            <div class="col col-md-6" id="computer">
                <div id="screen" class="ml">
                    <div class="container">
                        <div class="row" style="">
                            <div class="col-12">
                                <ul>
                                    <li class="brand">Logo</li>
                                    <li><a href="#">Links</a></li>
                                    <li><a href="#">Links</a></li>
                                    <li><a href="#">Links</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-5">
                                <img src="https://bnetcmsus-a.akamaihd.net/cms/template_resource/LAKZ6R7QEG6S1507822883033.svg" alt="philadelphia">
                            </div>
                            <div class="col-2">
                            </div>
                            <div class="col-5">
                                <img src="https://bnetcmsus-a.akamaihd.net/cms/template_resource/ZIVUVIWXNIFL1507822883114.svg" alt="Shangai dragon">
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-5">
                                2
                            </div>
                            <div class="col-2">
                                -
                            </div>
                            <div class="col-5">
                                2
                            </div>
                        </div>
                        <div class="space"></div>
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-8 twitch-player">
                                <img src="{{ asset('img/twitch.svg') }}" alt="">
                                <span>Overwatch Tournament by Snowkee</span> 
                            </div>
                            <div class="col-2"></div>
                        </div>
                    </div>
                </div>
                <div id="pre-base" class="ml">
                </div>
                <div id="base" class="ml"></div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
</body>
</html>