<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Restray">
    <meta name="description" content="S'inscrire à la bêta de Striker Tournament">
    <title>Striker Tournament - Inscription Beta</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.css">
    <link rel="stylesheet" href="{{ asset('css/sub.min.css')}}">
</head>

<body style="overflow-x:hidden;">
    <div class="container">
        <section class="d-flex flex-row justify-content-center align-items-center order-sm-1 order-md-1 order-lg-2" style="min-height:100vh;">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 d-flex justify-content-center align-items-center order-2 order-sm-2 order-md-1 order-lg-1 order-xl-1" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <form method="post" action="{{ route('subscribe-store') }}">
                        @csrf
                        <div class="form-group d-inline-block float-left sameline-field" style="margin-right:9%;">
                            <label>Prénom</label><input name="surname" class="form-control" type="text">
                            @if ($errors->has('surname'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('surname') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group d-inline-block float-right sameline-field">
                            <label>Nom</label><input name="name" class="form-control" type="text">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group d-inline-block" style="width:100%;">
                            <label>Adresse complète</label><input name="adress" class="form-control" type="text" placeholder="99 rue du tryhard, Paris 75099, France">
                            @if ($errors->has('adress'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('adress') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group d-inline-block" style="width:100%;">
                            <label>Adresse E-Mail</label><input name="email" class="form-control" type="email">
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group d-inline-block sameline-field" style="margin-right:8%;">
                            <label>Pseudo</label><input name="pseudo" class="form-control" type="text">
                            @if ($errors->has('pseudo'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('pseudo') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group d-inline-block sameline-field">
                            <label>Date de naissance</label><input name="birthday" class="form-control" type="date">
                            @if ($errors->has('birthday'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('birthday') }}
                                </div>
                            @endif
                        </div>
                        <button class="btn btn-outline-primary btn-block" type="submit" style="/*text-align:center;*/">S'inscrire</button></form>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 d-flex justify-content-center align-items-center order-1 order-sm-1 order-md-1 order-lg-2 order-xl-2" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                    <p class="lead text-left" style="font-family:Montserrat, sans-serif;">Striker tournament, l'un des meilleurs site de tournois que le monde est connu! Le rejoindre est faire partie de l'élite! Vous ne pouvez pas imaginer!</p>
                </div>
            </div>
        </section>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
    <script src="{{ asset('js/sub.min.js')}}"></script>
</body>

</html>