<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Restray">
    <meta name="description" content="Page de connection de Striker Tournament">
    <title>Striker Tournament - Connection</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/login.min.css') }}">
</head>

<body>
    <div id="login-one" class="login-one">
        <form class="login-one-form" method="post" action="{{ route('login') }}">
            @csrf
            <div class="col">
                <div class="login-one-ico"><i class="fa fa-unlock-alt" id="lockico"></i></div>
                <div class="form-group">
                    <div>
                        <h3 id="heading">Connection:</h3>
                    </div>
                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" type="text" placeholder="E-Mail" id="input">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="password" type="password" placeholder="Mot de passe" id="input">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <button class="btn btn-primary" type="submit" id="button" style="background-color:#007ac9;">Se connecter</button>
                </div>
            </div>
        </form>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
</body>

</html>