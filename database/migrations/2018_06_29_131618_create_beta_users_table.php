<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetaUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beta_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pseudo', 50)->unique();
            $table->string('email')->unique();
            $table->string('surname', 100);
            $table->string('name', 100);
            $table->text('adress');
            $table->date('birthday');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beta_users');
    }
}
