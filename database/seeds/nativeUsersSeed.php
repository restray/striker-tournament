<?php

use Illuminate\Database\Seeder;

class nativeUsersSeed extends Seeder
{
    /**
     * Create native users for the website
     *
     * @return void
     */
    public function run()
    {
        $user = new App\User();
        $pass = str_random(20);
        echo $pass;
        $user->password = Hash::make($pass);
        $user->email = 'contact@restray.org';
        $user->name = 'Restray';
        $user->save();
    }
}
